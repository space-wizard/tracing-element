//! Tools for creating spans for individual elements in a stream
//!
//! ```
//! use futures::stream::{self, StreamExt};
//! use tracing::{info, info_span};
//! use tracing_element::{ElementSpan, ElementSpanned, Recordable};
//!
//! // Create an element span which is used to coordinate recording of values and the
//! // overall scope of the spans
//! let element_span = ElementSpan::new();
//!
//! stream::iter(0i32..10)
//!     .record_element(&element_span, |p, i| {
//!         info_span!(parent: p, "my_span", value = i)
//!     })
//!     // After record_element and up until element_spanned stream operations will
//!     // will existing under `my_span`
//!     .filter_map(|i| async move {
//!         if i % 2 == 0 {
//!             info!("passed");
//!             Some(i)
//!         } else {
//!             info!("skipping");
//!             None
//!         }
//!     })
//!     // Instrument the stream. This is important as the record_element from above
//!     // will do nothing without this!
//!     .element_spanned(element_span)
//!     .for_each(|i| async move {
//!         // for_each not support yet so no span here :(
//!         info!("hi {}", i);
//!     });
//! ```
use std::{
    pin::Pin,
    sync::{Arc, Mutex},
    task::Poll
};

use futures::Stream;
use pin_project::pin_project;
use tracing::Span;

/// Records newly emitted elements
#[pin_project]
pub struct ElementInstrumentedStreamRecorder<S: Stream, G> {
    #[pin]
    inner: S,
    element_span: ElementSpan,
    buffered: Option<Option<S::Item>>,
    span_generator: G,
}

impl<S: Stream, G> ElementInstrumentedStreamRecorder<S, G> {
    fn new(stream: S, span: &ElementSpan, span_generator: G) -> Self {
        ElementInstrumentedStreamRecorder {
            inner: stream,
            element_span: span.clone(),
            buffered: None,
            span_generator,
        }
    }
}

impl<S, G> Stream for ElementInstrumentedStreamRecorder<S, G>
where
    S: Stream,
    S::Item: tracing::Value,
    G: Fn(Span, &S::Item) -> Span,
{
    type Item = S::Item;

    fn poll_next(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Option<Self::Item>> {
        let mut this = self.project();

        // If anything is in the buffer return that instead of polling inner
        if let Some(buffered) = this.buffered.take() {
            return Poll::Ready(buffered);
        }

        match this.inner.poll_next(cx) {
            Poll::Ready(maybe_value) => {
                let generator_ref = &mut this.span_generator;
                let parent_span = this.element_span.parent_span.clone();

                this.element_span.record_new_span(
                    maybe_value
                        .as_ref()
                        .map(move |value| generator_ref(parent_span, value)),
                );

                *this.buffered = Some(maybe_value);

                // Force another poll cycle so span can correctly be set for the entire time
                // TODO: I think this works correctly?
                cx.waker().wake_by_ref();
                Poll::Pending
            }
            Poll::Pending => Poll::Pending,
        }
    }
}

/// Applied to outer most stream to manage the scope of a potential element span
#[pin_project]
pub struct ElementInstrumentedStream<S> {
    #[pin]
    inner: S,
    element_span: ElementSpan,
}

impl<S> Stream for ElementInstrumentedStream<S>
where
    S: Stream,
    S::Item: tracing::Value,
{
    type Item = S::Item;

    fn poll_next(
        self: Pin<&mut Self>,
        cx: &mut std::task::Context<'_>,
    ) -> Poll<Option<Self::Item>> {
        let this = self.project();

        // Get the current element span if any
        let maybe_span = this.element_span.get_span();

        let ret = {
            let _guard = maybe_span.as_ref().map(|span| span.enter());
            this.inner.poll_next(cx)
        };

        ret
    }
}

/// Tracks current span
#[derive(Clone)]
pub struct ElementSpan {
    parent_span: Span,
    active_span: Arc<Mutex<Option<Span>>>,
}

impl ElementSpan {
    pub fn new() -> Self {
        ElementSpan {
            active_span: Arc::new(Mutex::new(None)),
            parent_span: Span::current(),
        }
    }

    pub fn record_new_span(&self, maybe_span: Option<Span>) {
        let mut active_span_lock = self.active_span.lock().unwrap();

        //dbg!("Recording", maybe_value.is_some());

        *active_span_lock = maybe_span
    }

    pub fn get_span(&self) -> Option<tracing::Span> {
        let active_span_lock = self.active_span.lock().unwrap();
        active_span_lock.clone()
    }
}

/// Stream extensions for recording `Stream::Item` into a span
pub trait Recordable: Stream + Sized {
    /// Record an element by generating a new span for it
    ///
    /// Use `Recordable::element_spanned` to determine the scope of the span
    fn record_element<F: Fn(Span, &Self::Item) -> Span>(
        self,
        element_span: &ElementSpan,
        f: F,
    ) -> ElementInstrumentedStreamRecorder<Self, F>;
}

impl<S> Recordable for S
where
    S: Stream,
{
    fn record_element<F: Fn(Span, &Self::Item) -> Span>(
        self,
        element_span: &ElementSpan,
        f: F,
    ) -> ElementInstrumentedStreamRecorder<Self, F> {
        ElementInstrumentedStreamRecorder::new(self, element_span, f)
    }
}

/// Stream extension for installing an ElementSpan
pub trait ElementSpanned: Stream + Sized {
    /// Instrument the stream with the provided `ElementSpan`
    ///
    /// A `Recordable::record_element` must be called for this to anything. This simply determines
    /// the scope of recorded span. This should be applied to the stream right before any operations that consume
    /// the stream such as `StreamExt::for_each`
    fn element_spanned(self, element_span: ElementSpan) -> ElementInstrumentedStream<Self>;
}

impl<S> ElementSpanned for S
where
    S: Stream,
{
    fn element_spanned(self, element_span: ElementSpan) -> ElementInstrumentedStream<Self> {
        ElementInstrumentedStream {
            inner: self,
            element_span,
        }
    }
}

