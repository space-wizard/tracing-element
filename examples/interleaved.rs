use std::{
    time::Duration,
};

use futures::{join, stream, StreamExt};
use tracing::{info, info_span};

use tracing_element::{ElementSpan, ElementSpanned, Recordable};

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    info!("starting");

    let stream = {
        let element_span = ElementSpan::new();

        stream::iter(0i32..10)
            .record_element(&element_span, |p, i| {
                info_span!(parent: p, "my_span", value = i)
            })
            .filter_map(|i| async move {
                if i % 2 == 0 {
                    info!("passed");
                    tokio::time::sleep(Duration::from_millis(100)).await;
                    Some(i)
                } else {
                    info!("skipping");
                    tokio::time::sleep(Duration::from_millis(100)).await;
                    None
                }
            })
            .element_spanned(element_span)
            .for_each(|i| async move {
                info!("hi {}", i);
            })
    };

    let stream2 = stream::iter(10i32..20)
        .filter_map(|i| async move {
            if i % 2 == 0 {
                info!("other passed");
                tokio::time::sleep(Duration::from_millis(100)).await;
                Some(i)
            } else {
                info!("other skipping");
                tokio::time::sleep(Duration::from_millis(100)).await;
                None
            }
        })
        .for_each(|i| async move {
            info!("other hi {}", i);
        });

    join!(stream, stream2);
}
